# Public docker repository for Kommunal Rapport

General purpose docker images adapted for our requirements. These images are
hooked up agaist docker hub. The following repositories are available:

  - **kommunalrapport/latex** LaTeX build environment that uses texlive and
    includes the XeLaTeX rendering engine. This image is available from
    ([Docker Hub](https://hub.docker.com/r/kommunalrapport/latex))

  - **kommunalrapport/mysql** Enhanced version of MySQL that also includes
    functionality enhancements. It has Robert Eisele's plugin
    [udf_infusion](https://github.com/infusion/udf_infusion) pre-installed.
    This image is available from ([Docker Hub](https://hub.docker.com/r/kommunalrapport/mysql))
